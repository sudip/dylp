#!/bin/sh

# called by uscan with '--upstream-version' <version> <file>
tar xzf $3
rm -rf DyLP-*/CoinUtils DyLP-*/Osi DyLP-*/Data
rm -f DyLP-*/DyLP/doc/dylp.pdf DyLP-*/DyLP/doc/dylp.ps
rm -rf DyLP-*/DyLP/doc/TexMF/fonts
rm -rf DyLP-*/DyLP/doc/TexMF/tex/latex/psnfss
#( cd DyLP-*/DyLP/doc && find ./ -regex '.*\.\(sty\|bib\|bst\)' -exec ln -s {} . \; )
rm -f $3
tar czf $3 DyLP-*
rm -rf DyLP-*

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $3 $origDir
  echo "moved $3 to $origDir"
fi

exit 0
